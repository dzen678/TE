

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh_TE_01_700.png)

&emsp;

## 连享会：效率分析专题 (TFP-SFA-DEA)

&emsp;

> &#x1F449; [最新版课纲 PDF](https://file.lianxh.cn/KC/lianxh_TE.pdf) &ensp; | &ensp;  [预习和FAQs](https://gitee.com/lianxh/TE/wikis)


<div STYLE="page-break-after: always;"></div>

&emsp;

## 1. 课程概览

本课程为期四天，系统介绍各类效率分析工具的原理和 Stata 实现方法。课程包括三个专题： **TFP 专题** (全要素生产率)、**SFA 专题** (随机边界分析) 和 **DEA 专题** (数据包络分析)。每个专题提供 1-2 篇论文的讲解，附带完整的重现资料，以便大家「借他山之石，攻己之玉」。

- **课程主页：** <https://gitee.com/lianxh/TE>
- **授课方式：** 线上直播 + 回放。
- **时间安排**：2021 年 5 月 15-16 日；5 月 21-22 日
  - **时段：** 上午 9:00-12:00; 下午 2:30-5:30；课后 30-60 分钟答疑
- **授课嘉宾：**
  - 龚斌磊 (浙江大学)，**TFP**，5.15 (周六)
  - 连玉君 (中山大学)，**SFA**，5.16 (周日)
  - 张&emsp;宁 (暨南大学)，**DEA**，5.21-22 (周五、周六)
- **软件/课件/参考文献**：
  - **授课软件：** Stata 软件。提供全套 Stata 实操程序、数据和 dofiles (开课前一周发送)
  - **参考文献：**：课程大纲中涉及的参考文献可直接在后文中单篇查看，亦可打包下载：[-Link1-](https://quqi.gblhgk.com/s/880197/KI4E0boi5C4JRX0q)，[-Link2-](https://www.jianguoyun.com/p/DQyvLGMQ1bn4CBj_leQD)
  - **课前预习和答疑：** [TE-Wiki-FAQs](https://gitee.com/lianxh/TE/wikis/Home)
- **报名链接：** <http://junquan18903405450.mikecrm.com/FhWLhS2>

&emsp;

## 2. 课程缘起

### 2.1 注重效率的「效率专题」课

有关「效率」的研究受到了越来越多的关注，这与中国经济转向高质量发展阶段密切相关。然而，转型过程任重道远。这意味着，对于效率的测算、影响因素，以及效率与经济、金融、国际贸易等各个领域的交叉影响都将成为未来很长一段时间的重要研究话题。

为此，连享会于 2020 年 5 月开设了「效率分析专题」课程，有 150 多位老师和同学参加，广受好评。以至于从去年 7 月开始，就不断有人追问：何时开设下一期？

我们针对助教团队和 20 余位学员做了课程反馈，形成了第二期「效率分析专题」课程，兼具「深度」和「广度」。我们的初衷是：把原理讲透，让各位有独立思考和分析的基础；同时又能涵盖一些相对前沿的方法和模型。

想达到让学生「**听懂+会用**」的效果并非易事。这需要老师能深入浅出地讲清楚模型的基本思想、适用条件，结果的解释和呈现，在文献中的应用情况，以及在实操中可能遇到的各种问题。此次授课的三位老师在过去的十年中都一直在使用各自主题中涵盖的模型和方法，有些程序甚至是我们自己编写的。因此，我们有信心让学生们「**听懂**」。那么，如何确保学生们「**会用**」呢？我们最终商议的方案是——案例教学-干中学。在每个专题中，我们都会精讲 1-2 篇期刊论文的 Stata 实现过程，伴以研究过程中各种思考和解决思路。课后，大家可以通过「精读论文+Stata 实操」的方式，逐步吸收、提升。

师资方面，用张宁老师的话说「这次讲座阵容很强大呀！」，不算夸张。

主讲 **DEA 专题** 的张宁老师是「**优青**」学者，在 **Science**、**Nature** 子刊、**Cell** 子刊、**经济研究** 等权威期刊发表论文 70 余篇，18 篇论文进入 ESI 热点和高被引论文。他多年来一直专注于资源环境经济，效率与生产率分析方面的问题，提出了多种改进的全要素生产率模型和能源环境效率模型。

主讲 **FTP 专题** 的龚斌磊老师是「**青年长江学者**」，是国际效率与生产率分析学会 (ISEAPA) 创始主席 Robin Sickles 教授的得意门生，博士论文获美国莱斯大学杰出博士论文奖。他长期关注整体经济、农业和能源行业的技术进步、生产率和增长核算等议题，成果见诸于 Journal of Development Economics, American Journal of Agricultural Economics 等期刊。

主讲 **SFA 专题** 的连玉君老师编写了双边随机边界模型的 Stata 实现程序，对于 SFA 各类模型的估算和应用有丰富的经验，曾出色完成国家税务总局委托项目「基于随机边界分析的税收流失率估算研究」，有多篇应用 SFA 的论文发表于**经济研究**、**金融研究** 等期刊。

在内容安排上，第一讲涵盖了有关 TFP (全要素生产率) 估算中的各种方法、陷进和最新进展，并结合主讲嘉宾龚斌磊老师发表的数篇论文的 Stata 重现过程来展示这些方法实际应用。

第二讲由连玉君老师介绍各类随机边界模型 (SFA)，包括 SFA 估算效率的基本原理、异质性 SFA，双边 SFA，以及新进发展出来的内生性 SFA。

第三、四讲由张宁老师系统介绍 DEA 在效率和生产率分析中的各类应用，并提供相应的 Stata 程序和实现过程。其中，有多个算法由张宁老师提出，并在同行中得到广泛应用。

### 2.2 课程特色

- **深入浅出**：掌握最主流的效率分析模型和方法：TFP，DEA，SFA
- **电子板书**：全程电子板书演示，课后分享；听课更专注，复习更高效。
- **讲义程序**：分享全套课件 (数据、程序和论文)，课程中的方法和代码可以应用于自己的论文中。

### 2.3 听课软件

- 本次课程可以在手机，ipad 以及 Windows 系统的电脑上听课。
- **特别提示：** 一个账号绑定一个设备，且听课电脑 **需要 Windows 系统** ，请大家提前安排自己的听课设备。
  - **手机/iPad** ：直接在应用商店搜索「大黄蜂云课堂」安装即可
  - **电脑 (windows)** ：打开 <https://www.360dhf.cn/dhfplayer/>，下载 **「大班课直播学生端」**。

<div STYLE="page-break-after: always;"></div>

&emsp;

## 3. 第 1 天：全要素生产率 (TFP) 专题

### 3.1 嘉宾简介

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%BE%9A%E6%96%8C%E7%A3%8A-%E5%B7%A5%E4%BD%9C%E7%85%A7100.png)

[龚斌磊](https://person.zju.edu.cn/gbl)，经济学博士，浙江大学公共管理学院研究员、博导，教育部青年长江学者。师从国际效率与生产率分析学会 (ISEAPA) 创始主席 Robin Sickles 教授，博士论文《多部门组织生产率及绩效分析》获美国莱斯大学杰出博士论文奖。研究领域属于应用计量与发展经济学，聚焦农业经济学、产业经济学、资源与环境经济学等方向，关注整体经济、农业和能源行业的技术进步、生产率和增长核算等议题。个人独著发表在 Journal of Development Economics, American Journal of Agricultural Economics 等期刊，合作论文发表在 Journal of Development Economics, Journal of Productivity Analysis 等期刊，并担任 JPE、JDE、AJAE 等二十余种 SSCI 期刊和《管理世界》、《经济学季刊》等中文期刊审稿专家。主持国家自科、教育部、农业农村部和省社科重大项目，撰写多份咨询报告获省部领导肯定性批示。

### 3.2 专题导引

什么是全要素生产率 (TFP)？为什么要研究 TFP？党的十九大报告作出我国经济已由高速增长阶段转向高质量发展阶段的重大判断，提出了提高全要素生产率的紧迫要求。TFP 反映了生产过程中各种投入要素的单位平均产出水平，即投入转化为最终产出的总体效率，在学术研究和政策研究中均具有重要地位。

TFP 的实证研究浩如烟海，如何才能脱颖而出、发表于顶尖期刊呢？这需要与经济学主流理论相结合。因此，有必要了解 TFP 的理论基础：经济增长理论。古典经济增长理论包括斯密式增长理论、李嘉图式增长理论和熊彼特式增长理论等。Solow 采用生产函数建立经济增长模型，奠定了新古典经济增长理论的基础。Romer 和 Lucas 开创了内生增长理论，认为技术外部性、人力和其他资本的溢出效应等因素能够内生地促进技术进步，保证经济不依赖外力也能实现持续增长。

那如何估计 TFP 呢？常规的，我们收集某个行业所有厂商的投入和产出数据，使用生产函数测算各厂商的 TFP。但对于微观企业层面的 TFP 估计而言，由于存在「同时性偏差」以及「样本选择偏差」等问题，使得 OLS 估计有偏，从而催生了 **FE** 方法、**OP** 方法、**LP** 方法、**ACF** 方法 等多种解决方案。以上方法存在什么区别？在实际应用中应该选择那种估计方案？往往令 TFP 实证研究者颇感困扰。

然而，随着经营多样化和企业集团化现象的出现，无法掌握所有厂商的投入和产出数据怎么办？以手机市场为例，需要测算生产函数和生产率，就要知道包括苹果、华为在内手机制造商该业务的投入产出情况。然而，手机业务只是华为运营商、企业和消费者三大业务中，消费者业务「1+8+N」中的「1」。在年报中，企业一般只披露企业整体投入产出数据，可能也会报告部门层面的产出 (业务分布与营收占比等) ，但不会披露部门层面投入情况 (如华为手机业务的职工数和资本) 。因此，无法测算手机市场的生产函数以及华为手机业务的生产率，无法与苹果等其他竞争者的进行比较。有两种方法解决该问题：一是通过建模分析多部门企业 (组织) 内部的资源配置情况，从而得到所需的投入产出数据；二是利用传统方法在企业层面直接进行估计，但在估计中考虑企业的异质性。

测算出 TFP 之后，还有什么花样可以玩，能够进一步研究其他那些问题？首先，可以分析 TFP 的影响因素，研究那些变量对影响 TFP 的水平。其次，增长核算法可以比较生产要素和 TFP 对经济增长的贡献大小，帮助判断经济增长模式。第三，测算出 TFP 后，可以进一步研究不同地区间 TFP 的收敛情况，分析后发地区/企业的赶超情况。

现有 TFP 研究还存在哪些缺陷，应该如何解决呢？将基于 2018-2021 年最新研究成果，讲授让审稿人耳目一新的 TFP 研究大杀器。同时，介绍如何将其他领域研究和 TFP 研究串联，以及未来 TFP 研究的热点领域和热点话题。

### 3.3 授课内容

- **全要素生产率研究：回顾与展望**
- **全要素生产率的概念**
  - 什么是全要素生产率？
  - 全要素生产率的重要性
  - 全要素生产率指标的主要应用领域
- **全要素生产率的理论基础**
  - 古典经济增长理论
  - 新古典经济增长理论
  - 内生增长理论
- **全要素生产率的测算方法**
  - 最小二乘法、固定效应方法
  - OP 法、LP 法、ACF 方法
  - 其他方法
- **多部门组织的全要素生产率**
  - 多部门组织生产率的重要性
  - 方案一：部门层面的分析
  - 方案二：组织层面的分析
- **全要素生产率的进一步应用**
  - 全要素生产率的影响因素分析
  - 增长核算法与新增长核算法
  - 生产率收敛性分析
- **全要素生产率的挑战与展望**
  - 如何选择模型？
  - 如何考虑空间相关性？
  - 如何识别驱动机制？

>**参考文献下载：** [PDF 原文集合](https://quqi.gblhgk.com/s/880197/KI4E0boi5C4JRX0q)，[-Link2-](https://www.jianguoyun.com/p/DQyvLGMQ1bn4CBj_leQD)

- Ackerberg, D., K. Caves, and G. Frazer. 2015. Identification Properties of Recent Production Function Estimators. **Econometrica** 83(6): 2411-2451. [-PDF-](https://file.lianxh.cn/Refs/TE/Gong/Ackerberg-2015-Econometrica.pdf)
- Chen, S., and B. Gong. 2021. Response and adaptation of agriculture to climate change: Evidence from China. **Journal of Development Economics** 148: 102557. [-PDF-](https://file.lianxh.cn/Refs/TE/Gong/Chen-2021-JDE.pdf)
- Gong, B. 2018a. Agricultural reforms and production in China changes in provincial production function and productivity in 1978–2015. **Journal of Development Economics** 132:18-31. [-PDF-](https://file.lianxh.cn/Refs/TE/Gong/Gong-2018a-JDE.pdf)
- Gong, B. 2018b. "Interstate competition in agriculture: Cheer or fear? Evidence from the United States and China." **Food Policy** 81:37-47. [-PDF-](https://file.lianxh.cn/Refs/TE/Gong/Gong-2018b-FP.pdf)
- Gong, B. 2020a. Agricultural productivity convergence in China. **China Economic Review** 60:101423. [-PDF-](https://file.lianxh.cn/Refs/TE/Gong/Gong-2020a-CER.pdf)
- Gong, B. 2020b. New Growth Accounting. **American Journal of Agricultural Economics** 102 (2):641-661. [-PDF-](https://file.lianxh.cn/Refs/TE/Gong/Gong-2020b-AJAE.pdf)
- Gong, B., and R. C. Sickles. 2020. Non-structural and structural models in productivity analysis: study of the British Isles during the 2007–2009 financial crisis. **Journal of Productivity Analysis** 53 (2):243-263. [-PDF-](https://file.lianxh.cn/Refs/TE/Gong/Gong-Sickles-2020-JPA.pdf)
- Gong, B., and R. C. Sickles. 2021. Resource allocation in multi-divisional multi-product firms. **Journal of Productivity Analysis** forthcoming. [-PDF-](https://file.lianxh.cn/Refs/TE/Gong/Gong-Sickles-2021-JPA.pdf)
- Levinsohn, James and Amil Petrin. 2003. "Estimating Production Functions Using Inputs to Control for Unobservables." **The Review of Economic Studies** 70 (2): pp. 317–341. [-PDF-](https://file.lianxh.cn/Refs/TE/Gong/Levinsohn_Petrin_2003_LP.pdf)
- Lin, Y. 1992. Rural Reforms and Agricultural Growth in China. **American Economic Review** 82 (1):34-51. [-PDF-](https://file.lianxh.cn/Refs/TE/Gong/Lin-1992-AER.pdf)
- Olley, G.S. and A. Pakes. 1996. "The dynamics of productivity in the telecommunications equipment industry." **Econometrica** 64 (6):1263–1297. [-PDF-](https://file.lianxh.cn/Refs/TE/Gong/Olley-Pakes-1996-OP.pdf)
- 鲁晓东, 连玉君. 中国工业企业全要素生产率估计:1999-2007[J]. **经济学:季刊**, 2012 (02):179-196. [-PDF-](https://file.lianxh.cn/Refs/TE/Gong/鲁晓东-连玉君-2012-经济学季刊-中国工业企业全要素生产率估计_1999-2007.pdf)

<div STYLE="page-break-after: always;"></div>

&emsp;

## 4. 第 2 天：随机边界分析 (SFA) 专题

### 4.1 嘉宾简介

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连玉君工作照100.JPG)

**[连玉君](http://lingnan.sysu.edu.cn/node/151)** ，经济学博士，副教授，博士生导师。2007 年毕业于西安交通大学金禾经济研究中心，现任教于中山大学岭南学院金融系。主讲课程为"金融计量"、"实证金融"等。已在《China Economic Review》、《经济研究》、《管理世界》、《经济学(季刊) 》、《金融研究》、《统计研究》等期刊发表论文 60 余篇；主持完成国家自然科学基金项目、教育部人文社科基金项目、广东自然科学基金项目等课题项目 10 余项。目前已完成 Panel VAR、Panel Threshold、Two-tier Stochastic Frontier 等计量模型的 Stata 实现程序，并编写过几十个小程序，如 `xtbalance`, `winsor2`, `bdiff`, `hausmanxt`, `ttable3`, `hhi5`, `ua`等。连玉君老师团队一直积极分享 Stata 应用经验，创办了公众号「Stata 连享会 (StataChina) 」，开设了 [「连享会-推文」](https://www.lianxh.cn)，[「连享会-知乎」](https://www.zhihu.com/people/arlionn) 两个专栏，累计阅读量超过 600 万人次。

### 4.2 专题导引

本讲系统介绍目前日益得到广泛应用的随机边界模型 (SFA)，包括异质性 SFA、面板 SFA 和双边 SFA，它们是产出效率、成本效率、议价能力、投资效率估算等领域的主要分析工具。

异质性随机边界模型有助于我们在估算出无效率程度的同时，可以进一步分析影响非效率程度的因素。

在前期文献中，固定效应面板 SFA 模型的估计是一直是个难题，本课程中提供了 [Greene (2005)](https://file.lianxh.cn/Refs/TE/Lian/Greene_2005a_TFE.pdf) 提出的 TFE-SFA (True fixed effect SFA) 模型，以及 [Wang and Ho (2010)](https://file.lianxh.cn/Refs/TE/Lian/Wang_Ho_2010_feSFA.pdf) 提出的 Scaling-TFE (Scaling True Fixed effect SFA) 模型的 Stata 估计程序，能够很方便地估计包含固定效应的面板 SFA 模型。

双边随机边界模型 (Two-tier SFA) 由 [Kumbhakar, Tsionas and Sipilinen (2009)](https://file.lianxh.cn/Refs/TE/Lian/Kumbhakar_2009_SFA.pdf) 提出，在衡量信息不对称程度、投资效率、议价能力等方面有重要应用，如 [卢洪友, 连玉君, 卢盛峰 (2011)](https://file.lianxh.cn/Refs/TE/Lian/连玉君_2011_经济研究_Twotier_SFA.pdf) 使用该模型测度了中国医疗市场的信息不对称程度， [任曙明和吕镯 (2014)](https://file.lianxh.cn/Refs/TE/Lian/任曙明_2014_管理世界_融资约束_政府补贴与全要素生产率.pdf) 使用该模型测度了有效生产率，进而估算了政府补贴的正效应和和融资约束的负效应。

完成 SFA 估计后，如何估计无效率程度在文献中也存在争议，本课程中提供了一个便捷的命令，可以得到多种文献中常用的无效率估计量，以便作对比分析或稳健性分析。

### 4.3 授课内容

- **传统 SFA 模型** (Traditional SFA)
- **SFA 的模型设定和估计方法**
- **异质性 SFA 模型** (Heterogeneity SFA)
  - 模型设定
  - 结果解读和效率估计
- **面板 SFA**
  - Grenne 难题
  - Greene ([2005](https://file.lianxh.cn/Refs/TE/Lian/Greene_2005a_TFE.pdf)), True Fixed Effect (TFE-SFA) 模型
  - [Wang and Ho (2010)](https://file.lianxh.cn/Refs/TE/Lian/Wang_Ho_2010_feSFA.pdf), SP-SFA 模型
- **双边 SFA 模型** (Two-tier SFA)
  - MLE 估计方法
  - 效率估算方法
  - **范例：** 卢洪友, 连玉君, 卢盛峰. 中国医疗服务市场中的信息不对称程度测算. **经济研究**, 2011(4): 94-106. [-PDF-](https://file.lianxh.cn/Refs/TE/Lian/连玉君_2011_经济研究_Twotier_SFA.pdf)
- **内生性 SFA 模型** (Endog-SFA, Karakaplan and Kutlu, [2017](https://file.lianxh.cn/Refs/TE/Lian/Karakaplan_2017_sfkk-理论部分.pdf); Karakaplan, [2017](https://file.lianxh.cn/Refs/TE/Lian/Karakaplan_2017_sfkk.pdf))

>**参考文献下载：** [PDF 原文集合](https://quqi.gblhgk.com/s/880197/KI4E0boi5C4JRX0q)，[-Link2-](https://www.jianguoyun.com/p/DQyvLGMQ1bn4CBj_leQD)

- Belotti, F., S. Daidone, G. Ilardi, V. Atella, **2013**, Stochastic frontier analysis using stata, **Stata Journal**, 13 (4): 719–758. [-PDF-](https://file.lianxh.cn/Refs/TE/Lian/Belotti_2013_SFA.pdf), [-PDF-v2-](https://file.lianxh.cn/Refs/TE/Lian/Belotti_2013_SFA_full.pdf)
- Greene, W., **2005**, Fixed and random effects in stochastic frontier models, **Journal of Productivity Analysis**, 23 (1): 7-32. [-PDF-](https://file.lianxh.cn/Refs/TE/Lian/Greene_2005a_TFE.pdf)
- Kumbhakar, S., E. Tsionas, T. Sipil inen, **2009**, Joint estimation of technology choice and technical efficiency: An application to organic and conventional dairy farming, **Journal of Productivity Analysis**, 31 (3): 151-161. [-PDF-](https://file.lianxh.cn/Refs/TE/Lian/Kumbhakar_2009_SFA.pdf)
- Kumbhakar, S. C., H. J. Wang, A. P. Horncastle, **2015**, A practitioner's guide to stochastic frontier analysis using stata, **Cambridge University Press**. [Link](http://www.cambridge.org/us/academic/subjects/economics/econometrics-statistics-and-mathematical-economics/practitioners-guide-stochastic-frontier-analysis-using-stata#zB81hXruYSwDM5JQ.97)
- Kumbhakar, S. C., C. F. Parmeter, V. Zelenyuk, 2017, Stochastic frontier analysis: Foundations and advances. Working paper. [-PDF-](https://file.lianxh.cn/Refs/TE/Lian/Kumbhakar_2017_SFA_Review.pdf)
- Habib, M., A. Ljungqvist, **2005**, Firm value and managerial incentives: A stochastic frontier approach, **Journal of Business**, 78 (6): 2053-2094. [-PDF-](https://file.lianxh.cn/Refs/TE/Lian/Habib_2005_JB_Het_SFA.pdf)
- Wang, H. J., C. W. Ho, **2010**, Estimating fixed-effect panel stochastic frontier models by model transformation, **Journal of Econometrics**, 157 (2): 286-296. [-PDF-](https://file.lianxh.cn/Refs/TE/Lian/Wang_Ho_2010_feSFA.pdf)
- 卢洪友, 连玉君, 卢盛峰, **2011**, 中国医疗服务市场中的信息不对称程度测算, **经济研究**, (4): 94-106. [-PDF-](https://file.lianxh.cn/Refs/TE/Lian/连玉君_2011_经济研究_Twotier_SFA.pdf)
- Karakaplan, M. U., & Kutlu, L. (2017). Handling Endogeneity in Stochastic Frontier Analysis. Economics Bulletin, 37(2), 889–901. [-PDF-](https://file.lianxh.cn/Refs/TE/Lian/Karakaplan_2017_sfkk-理论部分.pdf)
- Karakaplan, M. U., 2017, Fitting Endogenous Stochastic Frontier Models in Stata, Stata Journal, 17(1): 39–55. [-PDF-](https://file.lianxh.cn/Refs/TE/Lian/Karakaplan_2017_sfkk.pdf), [-PDF2-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700103)
- 任曙明, 吕镯, **2014**, 融资约束、政府补贴与全要素生产率——来自中国装备制造企业的实证研究, **管理世界**, (11): 10-23. [-PDF-](https://file.lianxh.cn/Refs/TE/Lian/任曙明_2014_管理世界_融资约束_政府补贴与全要素生产率.pdf)

<div STYLE="page-break-after: always;"></div>

&emsp;

## 5. 第 3-4 天：数据包络分析 (DEA) 专题

### 5.1 嘉宾简介

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/张宁工作照新100.jpg)

**张宁**，经济学博士，现任暨南大学经济学院教授、博士生导师，“科睿唯安”全球高被引科学家，国家优秀青年基金获得者，珠江青年学者，国家重点研发课题负责人。主要研究方向为：资源环境经济，效率与生产率分析等，提出多种改进的全要素生产率模型和能源环境效率模型。根据 Socpus 数据库统计，在非期望产出与环境效率领域 (Undesirable Outputs, Environmental Efficiency, T.3864) 排名全球顶尖学者第 2 位。现担任 SSCI 期刊《Social Science Journal》副主编；ABS 三星级期刊《Technological Forecasting and Social Change》期刊编委，以第一或通讯在 Science、Nature 子刊、Cell 子刊、《经济研究》等国内外期刊发表论文 80 余篇，20 篇论文进入 ESI 热点和高被引论文。主持 5 项国家基金项目、包括重点，优青，重大研究计划，国家重点研发课题，面上项目等，获得霍英东青年教师奖、国家优秀自费留学生等奖励。撰写的多篇研究报告获得各级领导批示，部分政策建议转换为政府决策。

### 5.2 专题导引

近年来，数据包络分析 (DEA) 方法，由于其不需要假定估计函数形式等优点，被广泛应用于环境与能源经济学、全要素生产率测算、银行效率和生产率评估等重要领域。

本讲系统介绍 DEA 在上述领域的效率和生产率分析中的应用，并提供相应的 Stata 程序和实现过程。

本讲首先介绍用于技术效率测算的基本径向效率模型 CCR 和 BCC，进而介绍非径向效率模型 SBM。DEA 模型由于缺少统计检验而受到一些批评，因此，我们还会介绍 Bootstrap 方法在 DEA 中的应用，以便估计效率值的置信区间。

全要素生产率被认为长期经济增长的核心动力，估计全要素生产率是经济学研究中重要问题。Färe et al. (1994) 首次将 DEA 模型运用到全要素生产率 (TFP) 的测算中，利用 Malmquist 指数测算 TFP，并将其分解为三个驱动因素。Ray and Desli (1997) 指出 Färe et al. (1994) 的分解错误，提出新的分解思路。 Färe et al. (1994) 提出的基于 DEA 的 TFP 测算，由于估计跨期距离函数会造成不可行解问题，Pastor and Lovell (2005) 提出了 Global Malmquist 指数来解决这一问题。考虑技术非退步假设，Shestalova, (2003) 提出了 Sequential Malmquist 指数，还有考虑分组异质性的 Metafroniter Malmquist 指数 (Oh and Lee, 2010)。本讲将详细介绍这些 TFP 指数的原理和 Stata 实现。

本讲还会介绍考虑非期望产出的方向距离函数模型 (DDF) 和非径向方向距离函数 (NDDF) 在环境效率和能源效率测算中的应用，并介绍其对偶模型 (Dual Model) 在影子价格和替代弹性方面的应用。在生产率方面，还将介绍基于 DDF 构建的 Malmquist-Luenberger (ML) 指数，以及 ML 指数的拓展模型: Global Malmquist-Luenberger (GML), Sequential Malmquist-Luenberger (SML) 和 Metafrontier Malmquist-Luenberger (MML) 指数的原理和 Stata 估计程序, 传统 DEA 构建的生产率模型由于缺乏统计推断，受到经济学者质疑，本讲座还将介绍基于 bootstrapping 的 ML 指数的估计和 Stata 程序。

最后，学以致用，我们会结合论文案例，讲解这些模型在环境能源经济和银行生产率估算中的应用。

### 5.3 授课内容

- **传统 DEA 模型 (CCR BCC)**
- **Bootstrapping DEA 模型**
- **Slack-based measure (SBM) 模型**
- **Malmquist index**
  - Malmquist 分解之争：FGNZ vs RD (Färe et al., 1994; Ray and Desli, 1997)
  - Global Malmquist Index (Pastor and Lovell, 2005; Shestalova, 2003)
  - Sequential Malmquist Index (Shestalova, 2003)
- **Directional distance function –方向距离函数**
  - Radial Measures (Chung et al., 1997) 径向方向距离函数
  - Non-radial DDF (Zhou et al., 2012, Zhang et al., 2014) 非径向方向距离函数
  - Metafroniter Non-radial DDF 共同边界的非径向方向距离函数 (Zhang et al., 2013)
  - SBM 方向距离函数在环境和能源效率评估中的应用 (陈诗一, 2012)
  - Non-radial DDF 的对偶模型 Dual Model 在影子价格和要素替代弹性方面的应用 (Zhang and Xie, 2015)
  - SBM 的对偶模型 Dual Model 在在影子价格和要素替代弹性方面的应用 (Zhang et al, 2015)
- **Malmquist-Luenberger Index**
  - Basic model (Chung et al., 1997)
  - Global Malmquist-Luenberger Index (Oh, 2010)
  - Sequential Malmquist-Luenberger Index (Oh and Heshmati, 2010)
  - Metafrontier Malmquist-Luenberger Index (Oh, 2010)
  - Energy Productivity Index (单要素的生产率动态指数构建, Li et al.,2015
  - Bootstraping Malmquist-Luenberger Index (Zhang et al., 2015)
- **Stata 范例**
  - 基于 SBM 的能源效率指数 (Zhang and Choi, 2013)
  - NDDF 和 SBM 对偶模型测算影子价格 (Zhang et al., 2015)
  - 基本模型在中国工业行业环境全要素生产率应用 (陈诗一，2010)
  - 在火电企业碳排放全要素生产率中的应用 (Zhang and Choi, 2013)
  - 在银行全要素生产率中的应用 (Fujii et al., 2014)

>**参考文献下载：** [PDF 原文集合](https://quqi.gblhgk.com/s/880197/KI4E0boi5C4JRX0q)，[-Link2-](https://www.jianguoyun.com/p/DQyvLGMQ1bn4CBj_leQD)

- Charnes, A., Cooper, W.W., Rhodes, E., 1978. Measuring the efficiency of decision making units. **European Journal of Operational Research**, 2, 429-444. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/Chames_1978_EHOR.pdf)
- Banker, R.D., Charnes, A., Cooper, W.W., 1984. Some Models for Estimating Technical and Scale Inefficiencies in Data Envelopment Analysis. **Management Science**, 30, pp. 1078-1092. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/Banker_1984_MS.pdf)
- Färe, R., Grosskopf, S., Norris, M., & Zhang, Z. (1994) . Productivity Growth, Technical Progress, and Efficiency Change in Industrialized Countries. **American Economic Review**, 84(1), 66-83. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/Fare_1994_AER.pdf)
- Ray, S., & Desli, E. (1997) . Productivity Growth, Technical Progress, and Efficiency Change in Industrialized Countries: Comment. **American Economic Review**, 87(5), 1033-1039. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/Ray_Desli_1997.pdf)
- Tone, K., 2001. A slacks-based measure of efficiency in data envelopment analysis. **European Journal of Operational Research**, 130, 498-509. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/Tone_2001.pdf)
- Pastor, J., Lovell, C. (2005) . A global Malmquist productivity index, **Economics Letters**, 88(2):266-271. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/Pastor_2005_EL.pdf) 
- Shestalova.V (2003) . Sequential malmquist indices of productivity growth: an application to oecd industrial activities. **Journal of Productivity Analysis**, 19(2-3), 211-226. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/Shestalova_2003_JPA.pdf)
- Chung Y, Färe R, Grosskopf S. 1997.Productivity and Undesirable Outputs: A Directional Distance Function Approach. **J Environ Manage;** 51:229-40. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/Chung_1997_JEM.pdf)
- Zhou P, Ang BW, Wang H. Energy and CO2 emission performance in electricity generation: A non-radial directional distance function approach. **European Journal of Operational Research** 2012;221:625-35. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/Zhou_2012_EJOR.pdf)
- Zhang N, Choi Y. Total-factor carbon emission performance of fossil fuel power plants in China: A metafrontier non-radial Malmquist index analysis. **Energy Economics** 2013; 40: 549-59. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/Zhang_Choi_2013_EE.pdf)
- Zhang N., Kong F., Choi Y., Zhou, P. 2014. The effect of size-control policy on unified energy and carbon efficiency for Chinese fossil fuel power plants. **Energy Policy**, 70, 193-200. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/Zhang_Kong_2014_EP.pdf)
- Zhang, N., Xie H. 2015. Toward Green IT: Modeling Sustainable Production Characteristics for Chinese Electronic Information Industry, 1980-2012. **Technological Forecasting and Social Change**, 96, 62-70. [-Link-](https://www.sciencedirect.com/science/article/pii/S0040162514002972), [-PDF-](http://sci-hub.ren/10.1016/J.TECHFORE.2014.10.011), [-PDF2-](https://file.lianxh.cn/Refs/TE/Zhang/Zhang-Xie-2015-Toward_Green_IT.pdf)
- Zhang, N., Kung, C., Zhou, P. 2015. Total-factor carbon emission performance of the Chinese transportation industry: A bootstrapped non-radial Malmquist index analysis. **Renewable and Sustainable Energy Reviews**, 41, 584-593. [-Link-](https://www.sciencedirect.com/science/article/pii/S136403211400762X), [-PDF-](http://sci-hub.ren/10.1016/J.RSER.2014.08.076), [-PDF2-](https://file.lianxh.cn/Refs/TE/Zhang/Zhang_Kung_Zhou_2015_RSER.pdf)
- Zhang, Ning, Fanbin Kong, and Chih-Chun Kung. 2015. On Modeling Environmental Production Characteristics: A Slacks-Based Measure for China’s Poyang Lake Ecological Economics Zone. **Computing in Economics and Finance**, 46 (3): 389–404. [-Link-](https://link.springer.com/article/10.1007/s10614-014-9467-2), [-PDF-](http://sci-hub.ren/10.1007/S10614-014-9467-2), [-PDF2-](https://file.lianxh.cn/Refs/TE/Zhang/Zhang-Kung-Zhou-2015-Total-factor_CEF.pdf)
- 陈诗一. (2010) . 中国的绿色工业革命:基于环境全要素生产率视角的解释(1980—2008) . **经济研究** (11), 21-34. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/陈诗一_2010JJYJ.pdf)
- 陈诗一. (2012) . 中国各地区低碳经济转型进程评估. **经济研究** (08), 33-45. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/陈诗一_2012JJYJ.pdf)
- Oh D. A global Malmquist-Luenberger productivity index. **Journal of Productivity Analysis** 2010;34: 183–97. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/Oh_2010_JPA.pdf)
- Oh D. A metafrontier approach for measuring an environmentally sensitive productivity growth index. **Energy Economics** 2010;32: 146-57. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/Oh_2010_EE.pdf)
- Oh D, Heshmati A. A sequential Malmquist–Luenberger productivity index: Environmentally sensitive productivity growth considering the progressive nature of technology. **Energy Economics** 2010;32: 1345-55. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/Oh_Heshmati_2010_EE.pdf)
- Fujii, H., Managi, S., Matousek, R. 2014, “Indian bank efficiency and productivity changes with undesirable outputs: A disaggregated approach. **Journal of Banking and Finance**”, Vol.38, 41-50. [-PDF-](https://file.lianxh.cn/Refs/TE/Zhang/Fujii_2014_JBF.pdf)

<div STYLE="page-break-after: always;"></div>

&emsp;

## 6. 报名和缴费信息

### 6.1 报名链接

- **主办方：** 太原君泉教育咨询有限公司
- **标准费用**(含报名费、材料费)：
  - 全价：4300 元/人
  - 团报价 (三人及以上)/老学员（现场班/线上直播大课学员）享受 9 折优惠
- **会员优惠：** 连享会会员可享 85 折优惠
- **Note：** 以上各项优惠不能叠加使用。
- **联系方式：**
  - 邮箱：[wjx004@sina.com](wjx004@sina.com)
  - 王老师：18903405450 (微信同号)
  - 李老师：‭18636102467 (微信同号)

> **[报名链接](http://junquan18903405450.mikecrm.com/FhWLhS2)：**  
>  [http://junquan18903405450.mikecrm.com/FhWLhS2](http://junquan18903405450.mikecrm.com/FhWLhS2)  
>  
> 或 长按/扫描二维码报名：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/二维码_TE_报名150.png "连享会-效率分析专题报名")

### 6.2 缴费方式

> **方式 1：对公转账**

- 户名：太原君泉教育咨询有限公司
- 账号：35117530000023891 (晋商银行股份有限公司太原南中环支行)
- **温馨提示：** 对公转账时，请务必提供「**汇款人姓名-单位**」信息，以便确认。

> **方式 2：微信扫码支付**

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/君泉收款码.png)

> **温馨提示：** 微信转账时，请务必在「添加备注」栏填写「**汇款人姓名-单位**」信息。

<div STYLE="page-break-after: always;"></div>

&emsp;

## 7. 诚聘助教

> ### 说明和要求

- **名额：** 8 名
- **任务：**
  - **A. 课前准备**：协助完成 3 篇推文，风格类似于 [lianxh.cn](https://www.lianxh.cn) 推文；
  - **B. 开课前答疑**：协助学员安装课件和软件，在微信群中回答一些常见问题；
  - **C. 上课期间答疑**：针对前一天学习的内容，在微信群中答疑 (8:00-9:00，19:00-22:00)；参见 [往期答疑](https://gitee.com/arlionn/PX/wikis/README.md?sort_id=3372005)。
  - Note: 下午 5:30-6:00 的课后答疑由主讲教师负责。
- **要求：** 热心、尽职，熟悉 Stata 的基本语法和常用命令，能对常见问题进行解答和记录。
- **特别说明：** 往期按期完成任务的助教自动获得本期助教资格，不必填写申请资料，直接联系连老师即可。
- **截止时间：** 2021 年 3 月 25 日 (将于 3 月 27 日公布遴选结果于连享会主页：[lianxh.cn](https://www.lianxh.cn))

> ### 申请链接：
>
> <https://www.wjx.top/jq/73021543.aspx>

> 或 扫码在线填写助教申请资料：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/二维码_TE_助教招聘150.png "连享会-效率分析专题-助教招聘")

&emsp; 

> **课程主页：** <https://gitee.com/lianxh/TE>   
> **课纲PDF：** <https://file.lianxh.cn/KC/lianxh_TE.pdf>

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)


&emsp;

&emsp;


## 相关课程

&emsp;

> ### &#x23E9; **2021 [五一论文班·精讲8篇论文](https://gitee.com/lianxh/text)**   
> &#x231A; 2021 年 5 月 2-4 日    
> &#x2B50; 主讲：梁平汉(中山大学)；张川川 (浙江大学)；连玉君 (中山大学)    
> &#x26EA; **课程主页**：[https://gitee.com/lianxh/paper](https://gitee.com/lianxh/paper)  

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh_paper_02_700.png)

&emsp; 

> ### &#x23E9; **2021 [生存分析专题](https://gitee.com/lianxh/ST)** (Survival Aanlysis)   
> &#x231A; 2021 年 4.24-25 (周六、周日)    
> &#x2B50; 主讲：王存同教授 (中央财经大学)   
> &#x26EA; **课程主页**：[https://gitee.com/lianxh/ST](https://gitee.com/lianxh/ST) | [微信版](https://mp.weixin.qq.com/s/SGrqJs7Ekfpn74jciFR9yw)  

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh_ST_01_700.png)

&emsp;



